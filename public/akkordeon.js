document.addEventListener("DOMContentLoaded", (_event) => {
    const akk = Array.from(document.getElementsByClassName("akkordeon"));

    akk.forEach(element => {
        element.addEventListener('click', handleClick);
        changeIndicator(element);
    });
});

function handleClick() 
{
    let content = this.nextElementSibling;
    content.classList.toggle('active');
    changeIndicator(this);
}

function changeIndicator(element) 
{
    let indicator = element.firstChild;

    if (element.nextElementSibling.classList.contains('active')) 
    {
        indicator.textContent = '▼';
    } else 
    {
        indicator.textContent = '▶';
    }

    element.style.color = (element.nextElementSibling.classList.contains('active')) ? '#D60042' : '#FEE23D';
}
